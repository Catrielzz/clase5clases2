package Clase5clases2;

public class PlataformaCrow {
    private String nombre;
    private double dineroPropio;
    private static double dineroTotal = 0.0;

    //Constructor de nombre
    public PlataformaCrow(String nombre) {
        this.nombre = nombre;
    }

    // Inicio Getters & Setters
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getDineroPropio() {
        return dineroPropio;
    }

    public void setDineroPropio(double dineroPropio) {
        this.dineroPropio = dineroPropio;
    }

    public static double getDineroTotal() {
        return dineroTotal;
    }

    public static void setDineroTotal(double dineroTotal) {
        PlataformaCrow.dineroTotal = dineroTotal;
    }
    // Fin Getters & Setters

    public void asignarDinero(double cantidadDinero){
        dineroTotal = dineroTotal - cantidadDinero;
        this.dineroPropio = cantidadDinero;
    }

    public static void imprimeDineroTotal(){
        System.out.println("La cantidad de dinero de la que se dispone es de: "+dineroTotal);
    }

    // Metodo para aumentar el dinero total
    public static void recogerDinero(double dinero){
        PlataformaCrow.dineroTotal += dinero;
    }
}
