package Clase5clases2;

public class Usuario {
    private String nombre;
    private double dDisponible;
    private int idUsuario;
    private static int numUsuarios;

    /* Este constructor contiene el nombre, dDisponible, idUsuario
       y suma 1 a numUsuarios cada vez que se cree un usuario*/
    public Usuario(String nombre, double dDisponible) {
        this.nombre = nombre;
        this.dDisponible = dDisponible;
        this.idUsuario = Usuario.getNumUsuarios();
        numUsuarios = numUsuarios + 1;
    }
    // Inicio Getters & Setters
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getdDisponible() {
        return dDisponible;
    }

    public void setdDisponible(double dDisponible) {
        this.dDisponible = dDisponible;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public static int getNumUsuarios() {
        return numUsuarios;
    }

    public static void setNumUsuarios(int numUsuarios) {
        Usuario.numUsuarios = numUsuarios;
    }
    // Fin Getters & Setters

    //Este metodo imprime el numero de usuarios
    public static void imprimeNumUsuarios(){
        System.out.println("El numero de ususarios es: " + Usuario.getNumUsuarios());
    }
    //Metodo para donar dinero
    public void donarTodoDinero(){
        PlataformaCrow.recogerDinero(this.dDisponible);
        this.setdDisponible(0.0);
    }
}
