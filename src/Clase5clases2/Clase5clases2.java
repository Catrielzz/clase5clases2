package Clase5clases2;

public class Clase5clases2 {

    public  static void main (String[] args){
        // Creacion de objetos usuario
        Usuario u1 = new Usuario("Yoel", 300.0);
        Usuario u2 = new Usuario("Ariel", 1.5);
        Usuario u3 = new Usuario("Facundo", 500.0);

        // Aplicacion de metodos(Usuario)
        Usuario.imprimeNumUsuarios();
        System.out.println("El usuario 2 tiene el ID: " + u2.getIdUsuario());

        // Creacion de objetos plataformas
        PlataformaCrow p1 = new PlataformaCrow("Save The Whales");
        PlataformaCrow p2 = new PlataformaCrow("Save The Harambee");

        // Donaciones
        u1.donarTodoDinero();
        u2.donarTodoDinero();
        u3.donarTodoDinero();

        // Dinero total en plataformacrow
        PlataformaCrow.imprimeDineroTotal();

        // Asignamos dinero a las plataformas
        p1.asignarDinero(500.0);
        p2.asignarDinero(1.5);

        // Dinero total en plataformacrow
        PlataformaCrow.imprimeDineroTotal();
    }
}
